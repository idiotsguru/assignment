import React, { Component } from "react";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authAction";
import "./home.css";
import ReactGA from "react-ga";
ReactGA.initialize("UA-127081367-1");
ReactGA.pageview(window.location.pathname + window.location.search);

class Home extends Component {
  componentWillMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  logOut = () => {
    window.closeOverlay();
    this.props.logoutUser(this.props.history);
  };

  render() {
    return (
      <section className="view intro-2">
        <div className="mask">
          <div className="container h-100 d-flex justify-content-center align-items-center">
            <div className="row pt-5 mt-5">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-body padding-bottom-40">
                    <div className="row mt-5 card-scroll">
                      <div className="col-md-12 ml-lg-12 ml-md-12">
                        <h5 className="text-center margin-top-20">
                          <strong>Profile</strong>
                        </h5>
                        <hr />
                        <div className="md-form">
                          <div className="text-center">
                            <b>Name</b>
                            <br />
                            {this.props.auth.user.name}
                          </div>
                        </div>
                        <div className="md-form">
                          <div className="text-center">
                            <b>Email</b>
                            <br />
                            {this.props.auth.user.email}
                          </div>
                        </div>
                        <hr />
                        <div
                          className="text-center"
                          onClick={this.logOut}
                          style={{ width: "350px" }}
                        >
                          <a className="btn btn-dark-green btn-rounded btn-block text-15">
                            Logout
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  { logoutUser }
)(Home);
