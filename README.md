# Assignment (Commerce Pundit)

This is the assignement given by Commerce Pundit for making MERN STACK project under following predefined rules.

1. Create [Signup page](http://localhost:3000/signup) using Node.js
2. Create [Login page](http://localhost:3000/login) using Node.js
3. Create [Profile page](http://localhost:3000/home) with Node.js
4. Make connection string using MongoDB
5. Create react components
6. Connect Backend system with React component

## Ruleset

1. Installing node.js and express generator.
2. Changing the application structure.
3. Restructuring views folder using partials.
4. Adding template for login, signup and profile.
5. Refactoring the app.js file and use middleware.
6. Creating Models folder, Adding a User Schema and protecting Routers.
7. Running the application and adding comments.
8. Dealing with Loopback boot filres.
9. Consuming the API.
10. Adding style, Routers and controller to the application.
11. Creating database tables using mongodb.

## System Requirement

1. Node Installation [Node.js](https://nodejs.org/en/download/) or update your npm (v6.4.1) and node (v10.15.0).
2. mongoDb Installation [MongoDB](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

## Source Code (_Develop Branch_)

1. Run `git clone -b master https://idiotsguru@bitbucket.org/idiotsguru/assignment.git [folder name]`.

## API Folder

1. Run `cd [folder name]`.
2. Run `npm install` this will load all the dependancy to your local computer.
3. Run `npm start` in `[folder name]`.

## FrontEnd Folder

1. Run `cd [folder name]/client`.
2. Run `npm install` this will load all the dependancy inside `client` folder.
3. Run `npm start` inside `client` folder.
