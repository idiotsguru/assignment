const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String
    },
    img_url: {
        type: String
    },
    fb_id: {
        type: String
    },
    gmail_id: {
        type: String
    }
});

module.exports = User = mongoose.model('users', UserSchema);
